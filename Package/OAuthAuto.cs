using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace PIT.Labs.ProxyServiceTest.OAuthClientLib
{
    public class OAuthAuto
    {
        /// <summary>
        /// user name used when confirming the authorization.
        /// </summary>
        public string UserName { get; set; }
            = System.Environment.GetEnvironmentVariable("TEST_OAUTH2_USERNAME");
        /// <summary>
        /// password used when confirming the authorization.
        /// </summary>
        public string Password { get; set; }
            = System.Environment.GetEnvironmentVariable("TEST_OAUTH2_PASSWORD");
        /// <summary>
        /// scoped requested for the authorization.
        /// </summary>
        public string Scopes { get; set; }
            = System.Environment.GetEnvironmentVariable("TEST_OAUTH2_SCOPES");
        /// <summary>
        /// client id used for requesting the authorization.
        /// </summary>
        public string ClientId { get; set; }
            = System.Environment.GetEnvironmentVariable("TEST_OAUTH2_CLIENTID");
        /// <summary>
        /// Method to create a web driver to simulate login during confirmation
        /// process.
        /// </summary>
        public Func<IWebDriver> WebDriverCreator { get; set; } = () =>
            {
                var firefoxPaths = new string[] {
                    @"C:\Program Files (x86)\Mozilla Firefox ESR\firefox.exe",
                    @"C:\Program Files\Mozilla Firefox\firefox.exe",
                    @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe"
                };

                var driverPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + @"\Selenium";

                foreach (var path in firefoxPaths)
                {
                    if (File.Exists(path))
                    {
                        try
                        {
                            var profile = new FirefoxProfile();
                            var options = new FirefoxOptions() { BrowserExecutableLocation = path, Profile = profile, UseLegacyImplementation = false };
                            var driver = new FirefoxDriver(driverPath, options);

                            return driver;
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine("s");
                        }
                    }
                }
                throw new FileNotFoundException("Could not find a valid FireFox path");
            };

        /// <summary>
        /// Uri of the OAuth endpoint.
        /// </summary>
        public Uri OAuthEndpointUri { get; set; } 
            = new Uri(System.Environment.GetEnvironmentVariable("OAUTH2_URL"));

        protected Uri OAuthTokenEndpoint
        {
            get
            {
                return new Uri(OAuthEndpointUri, "token");
            }
        }

        protected Uri OAuthCodeEndpoint
        {
            get
            {
                return new Uri(OAuthEndpointUri, "code");
            }
        }

        /// <summary>
        /// Use a WebDriver to confirm the authorization on the website. The 
        /// WebDriver will be created using the WebDriverCreator action.
        /// </summary>
        /// <param name="verifyUrl"></param>
        public void Confirm(string verifyUrl)
        {
            IWebDriver driver = null;
            try
            {
                driver = WebDriverCreator();
                driver.Navigate().GoToUrl(verifyUrl);

                IWebElement queryUser = driver.FindElement(By.Name("j_username"));
                queryUser.SendKeys(UserName);
                IWebElement queryPw = driver.FindElement(By.Name("j_password"));
                queryPw.SendKeys(Password);
                //queryPw.Submit();

                IWebElement submit = driver.FindElement(By.Name("_eventId_proceed"));
                submit.Click();

                var webDriverWait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

                webDriverWait.Until(d => d.FindElement(By.Name("btn")));

                IWebElement authorize = driver.FindElement(By.Name("btn"));
                authorize.Click();
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                if (driver != null)
                {
                    driver.Quit();
                    driver = null;
                }
            }
        }

        /// <summary>
        /// Request an authorization. The autorization has to be confirmed by
        /// the user and can then be completed.
        /// </summary>
        /// <returns>Dictionary with 'device_code' and 'user_code'</returns>
        public Dictionary<string, string> Request()
        {
            string postData = $"client_id={ClientId}&scope={Scopes}";

            using (WebResponse response = PostRequest(OAuthCodeEndpoint, postData))
            {
                Dictionary<string, string> responseFromServer = ReadResponse(response);

                return responseFromServer;
            }
        }

        /// <summary>
        /// Complete the authorization after the user has confirmed. The Method
        /// will block until the was confirmed by the user.
        /// </summary>
        /// <param name="deviceCode">Device code from the authorization request</param>
        /// <returns>Dictionary containing 'refresh_token' and 'access_token'</returns>
        public Dictionary<string, string> Complete(string deviceCode)
        {
            Dictionary<string, string> responseFromServer;

            string postData = $"client_id={ClientId}&code={deviceCode}&grant_type=device&device_name={DateTime.Now}";
            int tries = 0;
            do
            {
                if (tries > 5)
                {
                    throw new TimeoutException("Could not complete authorization after 5 retries.");
                }
                else if (tries > 0)
                {
                    Thread.Sleep(5100);
                }
                tries++;

                using (WebResponse response = PostRequest(OAuthTokenEndpoint, postData))
                {
                    responseFromServer = ReadResponse(response);
                }
            }
            while (responseFromServer["status"].Contains("error: authorization pending"));

            return responseFromServer;
        }

        /// <summary>
        /// Use the refresh token and get a fresh access token.
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns>an access token</returns>
        public string Refresh(string refreshToken)
        {
            string postData = $"client_id={ClientId}&refresh_token={refreshToken}&grant_type=refresh_token";
            using(WebResponse response = PostRequest(OAuthTokenEndpoint, postData))
            {
                return ReadResponse(response)["access_token"];
            }
        }

        /// <summary>
        /// Invalidate the supplied refresh token.
        /// </summary>
        /// <param name="refreshToken"></param>
        public void Invalidate(string refreshToken)
        {
            string postData = $"client_id={ClientId}&refresh_token={refreshToken}&grant_type=invalidate";
            using (WebResponse response = PostRequest(OAuthTokenEndpoint, postData))
            {
                ReadResponse(response);
            }
        }

        /// <summary>
        /// Convenience method combines creation, confirmation and completion 
        /// of an authorization and returns an refresh token.
        /// </summary>
        /// <returns>a refresh token</returns>
        public string CreateNewRefreshToken()
        {
            var authorizationRequest = Request();
            string verifyUrl = $"{authorizationRequest["verification_url"]}?q=verify&d={authorizationRequest["user_code"]}";

            Confirm(verifyUrl);

            var tokens = Complete(authorizationRequest["device_code"]);

            return tokens["refresh_token"];
        }

        private static WebResponse PostRequest(Uri codeEndpoint, string postData)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            WebRequest codeRequest = WebRequest.Create(codeEndpoint);
            codeRequest.Method = "POST";
            codeRequest.ContentType = "application/x-www-form-urlencoded";
            codeRequest.ContentLength = byteArray.Length;
            Stream dataStream = codeRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse response = codeRequest.GetResponse();
            return response;
        }
        private static Dictionary<string, string> ReadResponse(WebResponse response)
        {
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            responseStream.Close();
            response.Close();

            return System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string>>(responseFromServer);
        }
    }
}
