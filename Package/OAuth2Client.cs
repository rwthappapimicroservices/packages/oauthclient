using PIT.Labs.RestClientLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace PIT.Labs.ProxyServiceTest.OAuthClientLib
{
    public class OAuth2Client
    {
        private static RestClient restClient = new RestClient();

        private static string refreshToken = null;

        public static string RetrieveRefreshToken()
        {
            if(refreshToken == null)
            {
                var authorizationHelper = new OAuthAuto();
                refreshToken = authorizationHelper.CreateNewRefreshToken();
            }

            return refreshToken;
        }

        public static void InvalidateRefreshToken()
        {
            if (refreshToken != null)
            {
                var authorizationHelper = new OAuthAuto();
                authorizationHelper.Invalidate(refreshToken);
                refreshToken = null;
            }
        }

        public static string GetNewAccessToken()
        {
            return GetNewAccessToken(
                    System.Environment.GetEnvironmentVariable("TEST_OAUTH2_REFRESH_URL"),
                    RetrieveRefreshToken(),
                    System.Environment.GetEnvironmentVariable("TEST_OAUTH2_CLIENTID")
                    );
        }

        public static string GetNewAccessToken(string oauthUrl, string refreshToken, string clientId)
        {
            Uri tokenEndpoint = new Uri(oauthUrl);

            string postData = String.Format("client_id={0}&refresh_token={1}&grant_type=refresh_token", clientId, refreshToken);
            WebResponse response = PostRequest(tokenEndpoint, postData);
            Dictionary<string, string> responseFromServer = ReadResponse(response);

            return responseFromServer["access_token"];
        }
        
        public static string GetNewAnonymousAccessToken()
        {
            Uri tokenEndpoint = new Uri(System.Environment.GetEnvironmentVariable("TEST_OAUTH2_REFRESH_URL"));

            string postData = String.Format("client_id={0}&grant_type=anonymous",
                    System.Environment.GetEnvironmentVariable("TEST_OAUTH2_CLIENTID"));
            WebResponse response = PostRequest(tokenEndpoint, postData);
            Dictionary<string, string> responseFromServer = ReadResponse(response);

            return responseFromServer["access_token"];
        }

        public static string GetRPNSSendPNContextFromToken(string token)
        {
            string scope = System.Environment.GetEnvironmentVariable("OAUTH2_RPNS_SEND_PN_SCOPE");
            string serviceId = System.Environment.GetEnvironmentVariable("OAUTH2_RPNS_SEND_PN_SERVICEID");
            return GetContextFromToken(token, scope, serviceId);
        }

        public static string GetRPNSEditContextFromToken(string token)
        {
            string scope = System.Environment.GetEnvironmentVariable("OAUTH2_RPNS_EDIT_SCOPE");
            string serviceId = System.Environment.GetEnvironmentVariable("OAUTH2_RPNS_EDIT_SERVICEID");
            return GetContextFromToken(token, scope, serviceId);
        }

        private static string GetContextFromToken(string token, string scope, string serviceId)
        {
            Uri oauthEndpoint = new Uri(System.Environment.GetEnvironmentVariable("OAUTH2_CONTEXT_URL"));

            string query = String.Format("service_id={0}&service_scope={1}&access_token={2}", serviceId, scope, token);

            var context = restClient.HttpGetText(oauthEndpoint, query);

            return System.Text.Json.JsonSerializer.Deserialize<string>(context);
        }

        private static Dictionary<string, string> ReadResponse(WebResponse response)
        {
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            responseStream.Close();
            response.Close();

            return System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string>>(responseFromServer);
        }

        private static WebResponse GetRequest(Uri codeEndpoint, string query)
        {
            WebRequest codeRequest = WebRequest.Create(String.Format("{0}?{1}", codeEndpoint.OriginalString, query));
            codeRequest.Method = "GET";
            WebResponse response = codeRequest.GetResponse();
            return response;
        }

        private static WebResponse PostRequest(Uri codeEndpoint, string postData)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            WebRequest codeRequest = WebRequest.Create(codeEndpoint);
            codeRequest.Method = "POST";
            codeRequest.ContentType = "application/x-www-form-urlencoded";
            codeRequest.ContentLength = byteArray.Length;
            Stream dataStream = codeRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse response = codeRequest.GetResponse();
            return response;
        }
    }
}
